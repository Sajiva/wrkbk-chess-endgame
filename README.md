# README #

To compile the program:
javac GameUI.java EndGame.java
or
javac *.java

To run the program:
java GameUI <input file name>

Program output:
The solutions are printed to output file named "output.txt" as well as to the terminal.